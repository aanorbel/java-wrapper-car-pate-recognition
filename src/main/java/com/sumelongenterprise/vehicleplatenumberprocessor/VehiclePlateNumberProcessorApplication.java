package com.sumelongenterprise.vehicleplatenumberprocessor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

@SpringBootApplication
public class VehiclePlateNumberProcessorApplication implements CommandLineRunner {

    @Autowired
    private PlateNumberProcessor plateNumberProcessor;

    public static void main(String[] args) {
        SpringApplication.run(VehiclePlateNumberProcessorApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        plateNumberProcessor = new PlateNumberProcessorGateway();
        File photo = plateNumberProcessor.getResourceAsFile("picture.jpg");
        System.out.printf("File %s contains plate number : %s", photo, plateNumberProcessor.getPlateNumber(photo));
    }

}
