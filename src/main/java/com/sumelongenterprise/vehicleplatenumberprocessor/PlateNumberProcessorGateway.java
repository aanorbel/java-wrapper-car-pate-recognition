package com.sumelongenterprise.vehicleplatenumberprocessor;

import org.springframework.stereotype.Service;

import java.io.*;

@Service
public class PlateNumberProcessorGateway implements PlateNumberProcessor {
    @Override
    public String getPlateNumber(File file) {
        File mainScript = getMainScript();

        return executeCommand(" python " + mainScript.getAbsolutePath() + " " + file.getAbsolutePath());
    }

    private File getMainScript() {
        getResourceAsFile("classifications.txt");
        getResourceAsFile("DetectChars.py");
        getResourceAsFile("DetectPlates.py");
        getResourceAsFile("flattened_images.txt");
        getResourceAsFile("PossibleChar.py");
        getResourceAsFile("PossiblePlate.py");
        getResourceAsFile("Preprocess.py");
        return getResourceAsFile("Main.py");

    }

    @Override
    public File getResourceAsFile(String resourcePath) {

        try {
            InputStream in = ClassLoader.getSystemClassLoader().getResourceAsStream(resourcePath);
            if (in == null) {
                return null;
            }

            File tempFile = new File(resourcePath);
            tempFile.deleteOnExit();

            try (FileOutputStream out = new FileOutputStream(tempFile)) {
                //copy stream
                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = in.read(buffer)) != -1) {
                    out.write(buffer, 0, bytesRead);
                }
            }
            return tempFile;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String executeCommand(String command) {
        StringBuffer output = new StringBuffer();

        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.toString();

    }
}
