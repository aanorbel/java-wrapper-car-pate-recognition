package com.sumelongenterprise.vehicleplatenumberprocessor;

import java.io.File;

public interface PlateNumberProcessor {

    String getPlateNumber(File file);

    File getResourceAsFile(String resourcePath);
}
