# Vehicle Plate Number Processor Module

Process a picture and extract the plate number from it.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

* [Opencv](https://opencv.org/)
* [Python](https://www.python.org/)
* [JDK 8 ](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

### Installing

A step by step series of examples that tell you how to get a development env running

Installing Opencv and Python 

* [Windows](https://docs.opencv.org/3.4.1/d5/de5/tutorial_py_setup_in_windows.html)
* [Fedora / RedHat](https://docs.opencv.org/3.4.1/dd/dd5/tutorial_py_setup_in_fedora.html)
* [Ubuntu / Debian](https://docs.opencv.org/3.4.1/d2/de6/tutorial_py_setup_in_ubuntu.html)

## Running the tests

Explain how to run the automated tests for this system

### Running sample test

Explain what these tests test and why

```
mvn clean test
```
[Test File](src/test/java/com/sumelongenterprise/vehicleplatenumberprocessor/PlateNumberProcessorTest.java)
